package com.codingraja.test;

import java.util.Iterator;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.codingraja.domain.Address;
import com.codingraja.domain.Customer;

public class GetCustomer {

	public static void main(String[] args) {
		
		Configuration configuration = new Configuration();
		configuration.configure("hibernate.cfg.xml");
		
		SessionFactory factory = configuration.buildSessionFactory();
		
		Session session = factory.openSession();
		Customer customer = session.get(Customer.class, new Long(1));
		
		System.out.println("Customer ID: "+customer.getId());
		System.out.println("Customer Name: "+customer.getFirstName());
		
		List<Address> addresses = customer.getAddresses();
		Iterator<Address> itr = addresses.iterator();
		
		while(itr.hasNext()) {
			Address address = itr.next();
			System.out.println("Address ID: "+address.getId());
			System.out.println("City: "+address.getCity());
		}
		
		session.close();
	}

}
