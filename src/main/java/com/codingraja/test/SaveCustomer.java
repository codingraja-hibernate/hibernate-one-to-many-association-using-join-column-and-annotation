package com.codingraja.test;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.codingraja.domain.Address;
import com.codingraja.domain.Customer;

public class SaveCustomer {

	public static void main(String[] args) {
		
		Configuration configuration = new Configuration();
		configuration.configure("hibernate.cfg.xml");
		
		SessionFactory factory = configuration.buildSessionFactory();
		
		List<Address> addresses = new ArrayList<Address>();
		addresses.add(new Address("H-10", "MRA Building", "Bangalore", "Karnataka", 560068L));
		addresses.add(new Address("H-6", "2nd Floor, MRA Building", "Bangalore", "Karnataka", 560068L));
		
		Customer customer = new Customer("Coding", "RAJA", "info@codingraja.com", 9742900696L, addresses);
		
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		session.save(customer);
		transaction.commit();
		session.close();
		
		System.out.println("Customer Record saved successfully!");
	}

}
